<?php

$numbers = array(23, 56, 8, 50, 14, 67, 41, 18, 70, 33, 90, 2, 77, 48, 31, 65, 23, 95, 39, 82, 9, 53, 22);

$indices_of_max = array_keys($numbers, max($numbers));
$index_of_last_max = $indices_of_max[count($indices_of_max)-1];

print "Number of numbers before last max number is: " . $index_of_last_max;
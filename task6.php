<?php

$array = [1, 4, 7, 10, 13, 16, 19, 22];
$result = check($array);


if (isset($result)) {
    print "\nResult is $result\n";
} else {
    print "\nResult is NULL\n";
}

function check(array $array) {
    sort($array);
    $diff = $array[1] - $array[0];
    for($i = 0; $i < count($array)-1; $i++) {
        $b = current($array);
        $a = next($array);
        if ($a - $b !== $diff) {
            return null;
        }
    }
    return $diff;
}
<?php

$string_a = "hello";
$string_b = "hello";

$length = min(strlen($string_b), strlen($string_a));

if ($string_a===$string_b)
{
    print "\n2 strings are same\n";
}
else
{
    if (substr($string_b, 0, strlen($string_a)) === $string_a || substr($string_a, 0, strlen($string_b)) === $string_b)
    {
        print "\nFirst " . $length . " symbols are the same!\n";
    }
    else
    {
        for ($i = 0; $i < $length; $i++)
        {
            if ($string_a[$i] !== $string_b[$i])
            {
                print "\nFirst $i symbols are the same\n";
                break;
            }
        }
    }
}
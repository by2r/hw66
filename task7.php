<?php

print "Enter a number to get consecutive numbers that sum to that number: ";
$number = trim(fgets(STDIN));

if (!is_numeric($number)) {
    print "Enter a number!\n\tTry again!";
    die();
}

$remainder = 0;
$prev_rem = 0;

for ($i = 2; $i < $number; $i++) {
    $prev_rem = $prev_rem + 1;
    $remainder += $prev_rem;

    $num = ($number-$remainder)/$i;

    if ($num <= 0) {
        break;
    }

    $result = "$num";
    if ($num - floor($num) == 0) {
        for ($j = 1; $j < $i; $j++) {
            $result .= " + " . ($num + $j);
        }
        print "\nResult: " . $result . " = $number\n";
    }
}